#!/usr/bin/python3
import json
import os

def strip(s):
    return ' '.join(s.split()).replace("> <", "><")

chordir = "chords"
chords = []
authors = os.listdir(chordir)
authors.sort()
for author in authors:
    songs = []
    songlist = os.listdir(chordir + '/' + author)
    songlist.sort()
    for song in songlist:
        text = []
        for line in open(chordir + "/" + author + "/" + song):
            text.append(line.strip())
        songs.append({"name": song, "text": text})
    chords.append({"author": author, "songs": songs})

html = strip(open("template.html", "r").read())
style = strip(open("style.css", "r").read())
script = strip(open("main.js", "r").read())
gen = html.format(style=style, script=script, JSON=json.dumps(chords, ensure_ascii=False))
with open("chords.html", "w") as f:
    f.write(gen)