let g_state = "authors";
let g_author = -1;
let g_song = -1;
let div_main = document.getElementById("main");
let h1_title = document.getElementById("title");
function redraw() {
    div_main.innerText = "";
    switch (g_state) {
        case "authors": {
            h1_title.innerText = "Исполнители";
            for (i in chords) {
                let author = chords[i]["author"];
                let p = document.createElement("p");
                p.innerText = author;
                p.setAttribute("onclick", "show_songs(" + i + ")");
                div_main.appendChild(p);
            }
            break;
        }
        case "songs": {
            h1_title.innerText = chords[g_author]["author"];
            let btn_back = document.createElement("button");
            btn_back.innerText = "Back";
            btn_back.onclick = function() { g_state = "authors"; redraw(); };
            div_main.appendChild(btn_back);
            let songs = chords[g_author]["songs"];
            for (s in songs) {
                let song = songs[s]["name"];
                let p = document.createElement("p");
                p.innerText = song;
                p.setAttribute("onclick", "show_text(" + s + ")");
                div_main.appendChild(p);
            }
            break;
        }
        case "text": {
            h1_title.innerText += " - " + chords[g_author]["songs"][g_song]["name"];
            let btn_back = document.createElement("button");
            btn_back.innerText = "Back";
            btn_back.onclick = function() { g_state = "songs"; redraw(); };
            div_main.appendChild(btn_back);

            let text = chords[g_author]["songs"][g_song]["text"];
            for (line in text) {
                let div = document.createElement("div");
                line = text[line];
                if (line[0] == '#') {
                    div.classList = "chordline";
                    line = line.slice(1);
                } else {
                    div.className = "songline";
                }
                if (line.length == 0) {
                    line = " ";
                }
                div.innerText = line;
                div_main.appendChild(div);
            }
            break;
        }
    }
}

function show_songs(author) {
    g_state = "songs";
    g_author = author;
    redraw();
}

function show_text(song) {
    g_state = "text";
    g_song = song;
    redraw();
}